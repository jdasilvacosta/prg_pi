/*
Copyright 2021 j.dasilvacosta@usp.ceu.es

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/Licenses/LICENSE-2.0
	
Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
either express or implied. See the License for the specific
language governing permissions and limitations under the 
License.
*/
package matematicas;

public class Mates{
	public static double generarNumeroPi (double pasos){
		double areaCuadrado = 4;
		int aciertos = 0;

		for (int i = 1; i <= pasos; i++){
			double x = Math.random()*(1)+(-1);
			double y = Math.random()*(1)+(-1);
			double xs = x*x;
			double ys = y*y;
			if (Math.sqrt(xs + ys) <= 1){
				aciertos = aciertos + 1;
			}
		}

		int radio = 1;
		double areaCirculo = areaCuadrado*(aciertos/pasos);
		double estimacionPi = areaCirculo/(radio*radio);
		return estimacionPi;
	}
}
