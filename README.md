#Práctica 1 - Aproximación del numero pi por Montecarlo#

El objetivo de esta práctica es realizar un programa en código Java que genere una aproximación al numero pi mediante el método de Montecarlo.

Para realizarlo, basta con escribir `make ejecutar`. Si queremos una aproximación más o menos precisa, basta con cambiar el archivo `makefile` y editar el único número escrito para obtener un resultado distinto.
